FROM python:3.6
ENV PYTHONUNBUFFERED 1

COPY ./Pipfile Pipfile
COPY ./Pipfile.lock Pipfile.lock
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile

COPY . code
WORKDIR code

EXPOSE 8000
