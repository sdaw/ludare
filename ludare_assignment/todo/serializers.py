import datetime

from rest_framework import serializers

from .models import Todo, TodoState


class TodoSerializer(serializers.ModelSerializer):
    due_date = serializers.DateField(initial=datetime.date.today)
    state = serializers.ChoiceField(choices=TodoState.choices(), initial=TodoState.TODO.value)

    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(TodoSerializer, self).__init__(many=many, *args, **kwargs)

    def create(self, validated_data):
        return super(TodoSerializer, self).create(validated_data)

    class Meta:
        model = Todo
        fields = ('id', 'due_date', 'content', 'state')
        read_only_fields = ('id',)
