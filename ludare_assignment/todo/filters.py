from django_filters import DateFilter, ChoiceFilter
from django_filters.rest_framework import FilterSet

from .models import TodoState


class TodoFilterSet(FilterSet):
    due_date = DateFilter(
        'due_date', label='Due on',
        lookup_expr='contains'
    )

    state = ChoiceFilter(
        choices=TodoState.choices()
    )
