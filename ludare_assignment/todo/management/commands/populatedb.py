import logging

from django.core.management.base import BaseCommand

from ludare_assignment.todo.test.factories import TodoFactory

logger = logging.getLogger('django')


class Command(BaseCommand):
    help = 'Populate the db with sample todos'

    def add_arguments(self, parser):
        parser.add_argument('num_instances', nargs='?', type=int, default=10)

    def handle(self, *args, **options):
        num_instances = options.get('num_instances')
        TodoFactory.create_batch(num_instances)
        logger.info(f'Created {num_instances} todos')
