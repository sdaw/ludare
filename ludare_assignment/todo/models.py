import enum

from django.db import models


class ChoiceEnum(enum.Enum):
    @classmethod
    def choices(cls):
        return tuple((state.value, state.value) for state in cls)


class TodoState(ChoiceEnum):
    TODO = 'todo'
    IN_PROGRESS = 'in-progress'
    DONE = 'done'


class Todo(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField()
    due_date = models.DateField()
    state = models.CharField(choices=TodoState.choices(), max_length=12)

    def __str__(self):
        return self.id
