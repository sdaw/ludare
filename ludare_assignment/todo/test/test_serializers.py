import pytest
from django.forms.models import model_to_dict

from .factories import TodoFactory
from ..serializers import TodoSerializer


@pytest.fixture
def data():
    obj = TodoFactory()
    return model_to_dict(obj)


def test_serializer_empty():
    serializer = TodoSerializer(data={})
    assert not serializer.is_valid()


@pytest.mark.django_db
def test_serializer_valid(data):
    serializer = TodoSerializer(data=data)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_serializer_invalid_date(data):
    """Fail due to not being a datetime object"""
    data['due_date'] = '2018-13-01'
    serializer = TodoSerializer(data={})
    assert not serializer.is_valid()


@pytest.mark.django_db
def test_serializer_invalid_state(data):
    data['state'] = 'Invalid'
    serializer = TodoSerializer(data=data)
    assert not serializer.is_valid()
