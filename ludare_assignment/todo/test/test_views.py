import datetime
import random
from pathlib import Path
from urllib.parse import urljoin

import pytest
from django.core.management import call_command
from django.core.serializers.json import DjangoJSONEncoder
from django.forms.models import model_to_dict
from faker import Faker
from rest_framework import status
from rest_framework.test import RequestsClient
from rest_framework.utils import json

from ludare_assignment.config.common import BASE_DIR
from ludare_assignment.todo.models import Todo, TodoState
from ludare_assignment.todo.serializers import TodoSerializer
from ludare_assignment.todo.test.factories import TodoFactory

fake = Faker()


class PrefixedRequestsClient(RequestsClient):

    def __init__(self, prefix_url):
        self.prefix_url = prefix_url
        super(PrefixedRequestsClient, self).__init__()

    def request(self, method, url, *args, **kwargs):
        url = urljoin(self.prefix_url, url)
        return super(PrefixedRequestsClient, self).request(method, url, *args, **kwargs)


@pytest.fixture
def client():
    return PrefixedRequestsClient(prefix_url='http://test/api/v1/todo/')


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    """Load our test data"""
    with django_db_blocker.unblock():
        test_data_path = Path(BASE_DIR) / 'todo/test/test_data.json'
        call_command('loaddata', test_data_path)


@pytest.fixture
def due_date(django_db_setup):
    """Provide a random due date from existing todos"""
    random_date = random.choice(Todo.objects.values('due_date').distinct())
    date = random_date['due_date'].strftime('%Y-%m-%d')
    return date


class TodoJsonEncoder(DjangoJSONEncoder):
    """Allow JSON encoding of TodoState Enum"""

    def default(self, o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()
        if isinstance(o, TodoState):
            return o.value
        else:
            super(TodoJsonEncoder, self).default(o)


@pytest.mark.django_db
def test_get(client, django_db_setup):
    pk = 1
    response = client.get(f'{pk}/')
    obj = Todo.objects.filter(pk=pk).get()
    serializer = TodoSerializer(obj)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == serializer.data


@pytest.mark.django_db
def test_list(client, django_db_setup):
    response = client.get('')
    objs = Todo.objects.all()
    serializer = TodoSerializer(objs, many=True)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['results'] == serializer.data


@pytest.mark.django_db
def test_filter_state(client, django_db_setup):
    state = TodoState.TODO.value
    response = client.get(f'?state={state}')
    objs = Todo.objects.filter(state=state).all()
    serializer = TodoSerializer(objs, many=True)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['results'] == serializer.data


@pytest.mark.django_db
def test_filter_date(client, django_db_setup, due_date):
    response = client.get(f'?due_date={due_date}')
    objs = Todo.objects.filter(due_date=due_date).all()
    serializer = TodoSerializer(objs, many=True)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['results'] == serializer.data


@pytest.mark.django_db
def test_filter_date_and_state(client, django_db_setup, due_date):
    state = TodoState.TODO.value
    response = client.get(f'?due_date={due_date}&state={state}')
    objs = Todo.objects.filter(due_date=due_date, state=state).all()
    serializer = TodoSerializer(objs, many=True)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['results'] == serializer.data


@pytest.mark.django_db
def test_create_single_todo(client, django_db_setup):
    data = TodoFactory()
    response = client.post('', data=model_to_dict(data))
    assert response.status_code == status.HTTP_201_CREATED
    assert len(response.json())


@pytest.mark.django_db
def test_create_many_todo(client, django_db_setup):
    num_instances = 50
    objs = TodoFactory.create_batch(num_instances)
    data = [model_to_dict(obj) for obj in objs]
    headers = {'Content-type': 'application/json'}
    response = client.post('', data=json.dumps(data, cls=TodoJsonEncoder), headers=headers)
    assert response.status_code == status.HTTP_201_CREATED
    assert len(response.json()) == num_instances


@pytest.mark.django_db
def test_update_single(client, django_db_setup):
    pk = 1
    state = 'done'
    response = client.patch(f'{pk}/', data={'state': state})
    obj = Todo.objects.filter(id=pk).get()
    assert response.status_code == status.HTTP_200_OK
    assert obj.state == state


@pytest.mark.django_db
def test_update_many(client):
    client.headers.update({'X-BULK-OPERATION': 'true'})
    response = client.patch('', data={'state': 'done'})
    objs = Todo.objects.all()
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert all(obj.state == 'done' for obj in objs)


@pytest.mark.django_db
def test_delete_single(client, django_db_setup):
    pk = 1
    response = client.delete(f'{pk}/')
    obj = Todo.objects.filter(id=pk).first()
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert obj is None


@pytest.mark.django_db
def test_delete_many(client, django_db_setup):
    client.headers.update({'X-BULK-OPERATION': 'true'})
    response = client.delete('', data={'state': 'todo'})
    objs = Todo.objects.all()
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert all(obj.state != 'todo' for obj in objs)
