import datetime
import random

import factory

from ludare_assignment.todo.models import TodoState


class TodoFactory(factory.django.DjangoModelFactory):
    due_date = factory.Sequence(lambda n: datetime.date.today() + datetime.timedelta(days=n))
    content = factory.Sequence(lambda n: f'This is test todo number {n}')
    state = factory.LazyFunction(lambda: random.choice(list(TodoState)).value)

    class Meta:
        model = 'todo.Todo'
