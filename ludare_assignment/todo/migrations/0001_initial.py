from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Todo',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('content', models.TextField()),
                ('due_date', models.DateField()),
                ('state', models.CharField(choices=[('todo', 'todo'), ('in-progress', 'in-progress'), ('done', 'done')],
                                           max_length=12)),
            ],
        ),
    ]
