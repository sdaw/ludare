import os

if os.environ.get('DJANGO_CONFIGURATION') is None:
    os.environ['DJANGO_CONFIGURATION'] = 'Local'
from .local import Local
from .production import Production
