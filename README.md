# Ludare Todo Assignment

## Prerequisites

- Docker
- Docker-Compose
- SQLite

## Deployment
Create a new SQLite database
```bash
cd ludare_assignment
sqlite3 todo.db
> .quit
```

### Without Docker

Create a virtualenv
```bash
mkvirtualenv --python=python3.6 ludare
workon ludare
```

Install the python packages
```bash
pip install pipenv
pipenv install
```
* Change the secret key in .env

Source env
```bash
. ./.env
```

Run migrations
```bash
./manage.py migrate
```
Start the django server
```
(. ./.env && ./manage.py runserver 0.0.0.0:8000)
```

(Optional) Populate the database with sample todos
```bash
./manage.py populatedb
```
Run the tests
```bash
cd ludare_assignment
pytest
```

### With Docker
Build the docker image and start the server
```bash
docker-compose up --build
```

(Optional) Populate the database with sample todos
```bash
docker-compose run --rm web ./manage.py populatedb
```
Run the tests
```bash
docker-compose run --rm web pytest
```

## API
**Prefix**:
 `/api/v1/`

### Create new todos

**Request**:
`POST` `/todo/`

Parameters:
* Pass parameters as a dict to create a single todo or a list to create multiple

Name       | Type   | Required | Description
-----------|--------|----------|------------
due_date   | string | Yes      | The due date for the todo
content    | string | Yes      | The text content of the todo
state      | string | Yes      | The state of the todo

**Response**:
```json
Content-Type application/json
201 Created

{
  "id": 1,
  "due_date": "2018-12-07",
  "content": "test",
  "state": "todo"
}
```

### List all todos

**Request**:
`GET` `/todo/`

```json
Content-Type application/json
200 OK

{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 1,
      "due_date": "2018-12-07",
      "content": "This is test todo number 1",
      "state": "todo"
    },
    {
      "id": 2,
      "due_date": "2018-12-08",
      "content": "This is test todo number 2",
      "state": "in-progress"
    }
  ]
}
```

#### Filter todos by due date

**Request**:
`GET` `/todo/?due_date=:due_date`

Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
due_date   | string | Yes      | The due date for the todo

**Response**
```json
Content-Type application/json
200 OK

{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 7,
            "due_date": "2018-12-12",
            "content": "This is test todo number 6",
            "state": "todo"
        },
        {
            "id": 17,
            "due_date": "2018-12-12",
            "content": "This is test todo number 6",
            "state": "in-progress"
        }
    ]
}
```

#### Filter todos by state

**Request**:
`GET` `/todo/?state=:state`

Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
state      | string | Yes      | The state of the todo

**Response**
```json
Content-Type application/json
200 OK

{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 15,
      "due_date": "2018-12-10",
      "content": "This is test todo number 4",
      "state": "done"
    },
    {
      "id": 18,
      "due_date": "2018-12-13",
      "content": "This is test todo number 7",
      "state": "done"
    }
  ]
}
```

### Get a single todo

**Request**:
`GET` `/todo/:id`

Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
id         | int    | Yes      | The todo id

**Response**:

```json
Content-Type application/json
200 OK

{
  "id": 2,
  "due_date": "2018-12-07",
  "content": "This is test todo number 1",
  "state": "todo"
}
```

### Update Todos

* Single Todo
**Request**:
`PUT/PATCH` `/todo/:id`

Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
id         | int    | Yes      | The todo id

* Multiple todos
**Request**:
`PUT/PATCH` `/todo/`

* You must set header X-BULK-OPERATION in order to update multiple todos

Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
due_date   | string | No       | The due date for the todo
content    | string | No       | The text content of the todo
state      | string | No       | The state of the todo

**Response**:
```json
Content-Type application/json
200 OK

{
  "id": 1,
  "due_date": "2018-12-06",
  "content": "This is test todo number 0",
  "state": "done"
}
```

### Delete Todos

* Single Todo:
**Request**:
`DELETE` `/todo/:id`

Parameters:

Name       | Type   | Required | Description
-----------|--------|----------|------------
id         | int    | Yes      | The todo id

* Multiple todos:
**Request**:
`DELETE` `/todo/`

* You must set header X-BULK-OPERATION in order to delete multiple todos

```json
HTTP/1.1 204 No Content
```
